# this gem has no tests at all, so run a small example as test

require 'powerbar'

total = 10
step = 1
text = "Small test"
p = PowerBar.new
j = 0
(0..total).step(step).each do |i|
  p.show({:msg => text, :done => i, :total => :unknown})
  j += 1
  sleep 0.1
end
p.close
